const passwordField = document.querySelector(".password1");
const eyeIcon= document.querySelector("#eye");
eyeIcon.addEventListener("click", function(){
	this.classList.toggle("fa-eye-slash");
	const type = passwordField.getAttribute("type") === "password" ? "text" : "password";
	passwordField.setAttribute("type", type);
})

const passwordField2 = document.querySelector(".password2");
const eyeIcon2 = document.querySelector("#confirmEye");
eyeIcon2.addEventListener("click", function(){
	this.classList.toggle("fa-eye-slash");
	const type = passwordField2.getAttribute("type") === "password" ? "text" : "password";
	passwordField2.setAttribute("type", type);
})


function checkPassword() {
	let password1 = passwordField.value;
	let password2 = passwordField2.value;

	if (password1 === '')
		alert ("Please enter Password");


	else if (password2 === '')
		alert ("Please enter confirm password");

	else if (password1 !== password2) {
		alert ("Потрібно ввести однакові значення")
		return false;
	}

	else{
		alert("You are welcome!")
		return true;
	}
}

let btn = document.querySelector('.btn');
btn.onsubmit = function(event) {
	event.preventDefault();
}